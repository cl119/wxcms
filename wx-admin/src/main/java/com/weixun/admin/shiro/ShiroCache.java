package com.weixun.admin.shiro;

import com.weixun.comm.model.vo.Staff;
import com.weixun.model.SysMenu;
import com.weixun.model.SysRole;
import com.weixun.model.SysStaff;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zlx on 2017/12/13 0013.
 */
public class ShiroCache {

    public static final String LOGIN_USER = "loginUser";
    public static final String LOGIN_ROLE = "userRole";
    public static final String LOGIN_PERMISSION = "rolePermission";

    public static void setAttribute(Object key, Object value) {
        Subject account = SecurityUtils.getSubject();
        if (null != account) {
            Session session = account.getSession();
            if (null != session) {
                session.setAttribute(key, value);
            }
        }
    }

    public static Object getAttribute(Object key) {
        Subject account = SecurityUtils.getSubject();
        if (null != account) {
            Session session = account.getSession();
            if (null != session) {
                return session.getAttribute(key);
            }
        }
        return null;
    }

    public static Object removeAttribute(Object key) {
        Subject account = SecurityUtils.getSubject();
        if (null != account) {
            Session session = account.getSession();
            if (null != session) {
                return session.removeAttribute(key);
            }
        }
        return null;
    }

    public static boolean hasLogin() {
        return getAttribute(LOGIN_USER) != null ? true : false;
    }


    public static Staff getLoginUser() {
        return (Staff) getAttribute(LOGIN_USER);
    }
    public static void setLoginUser(Staff user) {
        setAttribute(LOGIN_USER, user);
    }

    public static Map<String,Object> getUserAll(){
        Subject account = SecurityUtils.getSubject();
        if (null != account) {
            Session session = account.getSession();
            if (null != session) {
                Map<String,Object> map = new HashMap<String,Object>();
                map.put(LOGIN_USER,session.getAttribute(LOGIN_USER));
                map.put(LOGIN_ROLE,session.getAttribute(LOGIN_ROLE));
                map.put(LOGIN_PERMISSION,session.getAttribute(LOGIN_PERMISSION));
                return map;
            }
        }
        return null;
    }

    public static void userLogout() {
        Subject account = SecurityUtils.getSubject();
        if (null != account) {
            Session session = account.getSession();
            if (null != session) {
                session.removeAttribute(LOGIN_USER);
                session.removeAttribute(LOGIN_ROLE);
                session.removeAttribute(LOGIN_PERMISSION);
            }
            account.logout();
        }
    }

    /**
     * 获取所有角色权限
     * @return
     */
    public static List<String> getRoles(){
        List<SysRole> roles = (List<SysRole>) getAttribute(LOGIN_ROLE);
        if(roles!=null){
            List<String> list = new ArrayList<>();
            for (SysRole role : roles) {
                list.add(role.getRoleName());
            }
            return list;
        }
        return  null;
    }

    /**
     * 获取所有 资源权限路径
     * @return
     */
    public static List<String> getPermissions(){
        List<SysMenu> permissions = (List<SysMenu>)getAttribute(LOGIN_PERMISSION);
        if(permissions!=null){
            List<String> list = new ArrayList<>();
            for (SysMenu permission : permissions) {
                list.add(permission.getMenuUrl());
            }
            return list;
        }
        return  null;
    }

    /**
     * 获取用户基础信息
     * @return
     */
    public static Map<String,Object> getUserInfo(){
        SysStaff user = (SysStaff) getAttribute(ShiroCache.LOGIN_USER);
        List<SysRole> roles = (List<SysRole>) getAttribute(LOGIN_ROLE);

        List<String> roleList = new ArrayList<String>();
        for (int i = 0; i < roles.size(); i++) {
            roleList.add(roles.get(i).getRoleName());
        }
        //用户基础信息
        Map<String, Object> userInfo = new HashMap<String, Object>();
//        userInfo.put("userAccount", user.getLoginAccount());
//        userInfo.put("userName", user.getUserName());
//        userInfo.put("userHead", user.getUserHead());
//        userInfo.put("userBirthday", user.getUserBirthday());
//        userInfo.put("userEmail", user.getUserEmail());
//        userInfo.put("userPhone", user.getUserPhone());
//        userInfo.put("userSex", user.getUserSex());
        userInfo.put("roles", roleList);
        return userInfo;
    }

    /**
     * 根据权限菜单生成树形菜单
     */
//    public static List<MenuTree> getMenus(){
//        List<SysPermission> permissions = (List<SysPermission>) ShiroCache.getAttribute(ShiroCache.LOGIN_PERMISSION);
//        Map<String,MenuTree> menuMap = new HashMap<String,MenuTree>();//一级菜单
//        List<MenuTree> items = new ArrayList<MenuTree>();//二级菜单
//        for (SysPermission per:permissions) {
//            String menuCode = per.getMenuCode();
//            String parentCode = per.getParentMenucode();
//            String menuType = per.getMenuType();
//            if("1".equals(menuType)){
//                MenuTree treeItem = new MenuTree();
//                treeItem.setMenuCode(per.getMenuCode());
//                treeItem.setMenuName(per.getMenuName());
//                treeItem.setClassIcon(per.getMenuClass());
//                treeItem.setDataUrl(per.getDataUrl());
//                treeItem.setSequence(per.getSequence());
//                if(StringUtil.isEmpty(parentCode)){
//                    menuMap.put(menuCode,treeItem);
//                }else{
//                    treeItem.setParentCode(per.getParentMenucode());
//                    items.add(treeItem);
//                }
//            }
//        }
//
//        for (MenuTree item:items){
//            MenuTree treeItem = menuMap.get(item.getParentCode());
//            if(treeItem!=null){
//                List<MenuTree> menus = treeItem.getMenus();
//                if(menus==null){
//                    menus = new ArrayList<MenuTree>();
//                    treeItem.setMenus(menus);
//                }
//                menus.add(item);
//            }
//        }
//        List<MenuTree> menus = new ArrayList<MenuTree>();
//        for (String key :menuMap.keySet()){
//            menus.add(menuMap.get(key));
//        }
//        return menus;
//    }
}
