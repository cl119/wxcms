package com.weixun.admin.controller;

import com.alibaba.fastjson.JSON;
import com.jfinal.json.JFinalJson;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.weixun.admin.model.vo.RolesMenu;
import com.weixun.admin.model.vo.TreeView;
import com.weixun.admin.service.RolesMenuService;
import com.weixun.comm.controller.BaseController;
import com.weixun.model.SysRole;
import com.weixun.admin.service.RoleService;
import com.weixun.utils.ajax.AjaxMsg;

import java.util.ArrayList;
import java.util.List;

public class RoleController extends BaseController
{
    RoleService roleService = new RoleService();
    RolesMenuService rolesMenuService = new RolesMenuService();

    /**
     * 保存方法
     */
    public void save()
    {
//        getModel(SysRole.class).save();
        AjaxMsg ajaxMsg = new AjaxMsg();
        boolean res = false;

        try {

            SysRole sysrole = getModel(SysRole.class,"");
            if (sysrole.getRolePk() != null && !sysrole.getRolePk().equals(""))
            {
                //更新方法
                res = sysrole.update();
            }
            else {
                //保存方法
                res = sysrole.save();
            }
            if(res)
            {
                ajaxMsg.setState("success");
                ajaxMsg.setMsg("保存成功");
            }
            else
            {
                ajaxMsg.setState("fail");
                ajaxMsg.setMsg("保存失败");
            }
//        getBean(Staff.class).save();
        }catch (Exception e)
        {
            e.getMessage();
            ajaxMsg.setState("fail");
            ajaxMsg.setMsg("保存失败");
        }
        renderJson(ajaxMsg);
    }


    /**
     * 更新数据
     */
    public void update()
    {
//        getModel(SysRole.class).update();
        AjaxMsg ajaxMsg = new AjaxMsg();
        boolean res = false;

        try {

            SysRole sysrole = getModel(SysRole.class,"");
            if (sysrole.getRolePk() != null && !sysrole.getRolePk().equals(""))
            {
                //更新方法
                res = sysrole.update();
            }
            else {
                //保存方法
                res = sysrole.save();
            }
            if(res)
            {
                ajaxMsg.setState("success");
                ajaxMsg.setMsg("更新成功");
            }
            else
            {
                ajaxMsg.setState("fail");
                ajaxMsg.setMsg("更新失败");
            }
//        getBean(Staff.class).save();
        }catch (Exception e)
        {
            e.getMessage();
            ajaxMsg.setState("fail");
            ajaxMsg.setMsg("更新失败");
        }
        renderJson(ajaxMsg);
    }

    /**
     * 删除数据
     */
    public void delete()
    {
        AjaxMsg ajaxMsg = new AjaxMsg();
        String staff_pk = this.getPara("role_pk");
        int res =roleService.deleteById(staff_pk);
        if (res >0) {
            ajaxMsg.setState("success");
            ajaxMsg.setMsg("删除成功");
        }
        else
        {
            ajaxMsg.setState("fail");
            ajaxMsg.setMsg("删除失败");
        }

        renderJson(ajaxMsg);
    }

    
    /**
     * 获取列表
     */
    public void list(){
        String role_pk = getPara("role_pk");
        List<Record> list= roleService.findList(role_pk);
        renderJson(JFinalJson.getJson().toJson(list));
    }

    /**
     * 基于layui的分页
     */
    public void pages(){
//        SysRole sysrole = getModel(SysRole.class,true);
        String role_name = getPara("role_name");
        int pageNumber = getParaToInt("page");
        int pageSize = getParaToInt("limit");
        //获得用户信息
        Page<Record> list = roleService.paginate(pageNumber,pageSize,role_name);
        renderPageForLayUI(list);
    }


    /**
     * 保存角色信息
     * @param
     */
    public void saveRole(){
        String role_pk = getPara("role_pk");
        String datas = getPara("datas");
        //赋权之前删除之前角色的数据
        boolean result=false;
        AjaxMsg ajaxMsg = new AjaxMsg();
        int res = rolesMenuService.deleteByIds(role_pk);
        if (res>=0) { //返回值为0是考虑到第一次给该角色赋权时可能不会删除任何数据
            List<TreeView> menus = JSON.parseArray(datas, TreeView.class);
            if (menus !=null && menus.size()>0)
            {
                result= this.saveData(menus,role_pk);
            }
        }
        if(result) {
            ajaxMsg.setMsg("保存成功!");
        } else {
            ajaxMsg.setMsg("保存失败!");
        }
        renderJson(ajaxMsg);
    }

    /**
     * 通用保存递归方法
     * @param menus
     * @param role_pk
     * @return
     */
    public boolean saveData(List<TreeView> menus,String role_pk)
    {
        boolean result=false;
        try {
            for (TreeView menu : menus) {
                RolesMenu rolesMenu = new RolesMenu();
                rolesMenu.setFk_roles_pk(role_pk);
                rolesMenu.setFk_menu_pk(menu.getId());
                rolesMenu.setChecked(menu.isChecked().equals("true")?"1":"0");
                rolesMenuService.save(rolesMenu);
                if (menu.getChildren()!=null&&menu.getChildren().size()>0)
                {
                    saveData(menu.getChildren(),role_pk);
                }
            }
            result=true;
        }catch (Exception e)
        {
            e.getStackTrace();
            result=false;
        }
        return  result;
    }

}
