<!doctype>
<html>
<head>
    <meta charset="gb2312">
    <title>青海省人力资源和社会保障厅</title>
    <link href="/css/index.css" rel="stylesheet">
    <link href="/css/zixuncenter.css" rel="stylesheet">
    <link href="/css/tp.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/js/gd/jquery.pack.js"></script>
    <script type="text/javascript" src="/js/jquery.SuperSlide.js"></script>
</head>
<body>
<!--header start-->
<!--#include virtual="/includes/top_zx.html" -->
<!--nav end-->
<!--body start-->

<!--资讯中心-->
<div class="Information">

        <div class="cur_pos" style="margin-top:0;"><span>当前位置</span> <a id="cus" href="">资讯中心</a><a href="">${channel.channel_name}</a></div>

    <div class="new_jobs_con">
        <div class="new_jobs_title"><span>发布日期</span>文件标题</div>
        <ul>
            <#if articlelist?exists>
                <#list articlelist as article>
                        <li>
                            <span>
                                ${article.article_sendtime}
                            </span>
                            <a href="${channel.channel_pages+article.article_pk+'.html?frompage=homepage'}" target="_blank">
                                <#if article.article_title?length gt 100>
                                ${article.article_title?substring(0,100)}...
                                <#else>
                                ${article.article_title}
                                </#if>
                            </a>
                        </li>
                </#list>
            </#if>
        </ul>

        <div class="clear"></div>

        <div class="page">


            <div class="fenye" style="padding-left:20px;">
                <#if page.pageNum  == 2>
                    <a href="${'index.html'}">首页</a>
                    <a href="${'index.html'}">上页</a>
                </#if>
                <#if page.pageNum gt 2>
                    <a href="${'index.html'}">首页</a>
                    <a href="${'index'+page.prePage+'.html'}">上页</a>
                </#if>
                <#if page.pageNum<page.pages>
                    <a href="${'index'+page.nextPage+'.html'}">下页</a>
                    <a href="${'index'+page.lastPage+'.html'}">末页</a>
                </#if>
            </div>
            <div class="fenye_right" style="padding-left:20px;">
                <div class="left"> 共有${page.total}条信息 &nbsp; &nbsp;${page.pageNum}/${page.pages}&nbsp;&nbsp;</div> &nbsp;&nbsp;
                &nbsp;&nbsp;
                转到第
                <select style="border:none; margin-top:5px;" name="currentpage" onChange="window.location.href='index'+ this.value + '.html'">
                <#--freemarker数组-->
                    <#--"navigatepageNums":[1,2,3,4,5]-->
                    <#list page.navigatepageNums as number>
                        <#--选中的页数等于实际页数则显示选中-->
                        <#if number == page.pageNum>
                            <option value="${number}" selected>${number}</option>
                        <#--选中的页数为1时，设置值为空，避免出现index1.html 而出现错误-->
                        <#elseif number == 1>
                            <option value="">${number}</option>
                        <#--大于1时正常显示-->
                        <#elseif number gt 1>
                            <option value="${number}">${number}</option>
                        </#if>

                    </#list>
                </select>
                页
            </div>
        </div>

    </div>


</div>


<!--body end-->
<!--footer start-->
<!--#include virtual="/includes/footer.html" -->
<!--footer end-->
<!--start-->
<!--end-->
</body>
</html>